# Sigrok PulseView softver

PulseView predstavlja logički analizator kao i osciloskop za Sigrok. Koristi se za jako brzo i efikasno dekodovanje komunikacije određenih protokola, a "user-friendly" grafičko okruženje ga čini veoma jednostavnim za korištenje. Nudi širok spektar dekodera za razne interfejse, stoga može biti od velike koristi prilikom komuniciranja između raznih uređaja.

# Instalacija PulseView softvera i neophodnih biblioteka

Instalacija PulseView-a za Ubuntu platformu je opisana u nastavku dokumenta.

## Instalacija biblioteka

U `/home` direktorijumu korisnika na razvojnoj Linux platformi kreirati novi direktorijum:
~~~
mkdir SigrokPV_Libs
~~~

pozicionirati se u kreirani direktorijum:
~~~
cd SigrokPV_Libs
~~~

zatim izvršiti sljedće komande za instalaciju biblioteke libsigrok:

- ### libsigrok biblioteka

	~~~
	sudo apt-get install git-core gcc g++ make autoconf autoconf-archive \
	automake libtool pkg-config libglib2.0-dev libglibmm-2.4-dev libzip-dev \
	libusb-1.0-0-dev libftdi1-dev libieee1284-3-dev libvisa-dev nettle-dev libavahi-client-dev \
	libhidapi-dev check doxygen python-numpy python-dev python-gi-dev python-setuptools swig default-jdk
	~~~

	##### Building
	~~~
	git clone git://sigrok.org/libsigrok
	cd libsigrok
	./autogen.sh
	./configure
	make
	sudo make install
	~~~

	Po završetku se vratiti u direktorijum SigrokPV_Libs korištenjem:
	~~~
	cd ..
	~~~

	zatim instalirati biblioteku libsigrokdecode:

- ### libsigrokdecode biblioteka

	~~~
	sudo apt-get install git-core gcc make autoconf automake libtool pkg-config libglib2.0-dev python3-dev
	~~~

	##### Building
	~~~
	git clone git://sigrok.org/libsigrokdecode
	cd libsigrokdecode
	./autogen.sh
	./configure
	make
	sudo make install
	~~~


## PulseView instalacija

Nakon instalacije neophodnih biblioteka može se instalirati PulseView.
U Home direktorijumu korisnika na Linux platformi kreirati novi direktorijum:

~~~
mkdir SigrokPulseView
~~~

zatim se pozicionirati u kreirani direktorijum:
~~~
cd SigrokPulseView
~~~

nakon toga izvršiti sljedeće komande:

~~~
sudo apt-get install git-core g++ make cmake libtool pkg-config \
libglib2.0-dev libboost-test-dev libboost-serialization-dev \
libboost-filesystem-dev libboost-system-dev libqt5svg5-dev qtbase5-dev\
qttools5-dev
~~~

##### Building
~~~
git clone git://sigrok.org/pulseview
cd pulseview
cmake .
make
sudo make install
~~~

Po završetku instalacije program se iz instalacionog direktorijuma može pokrenuti pomoću komande:

~~~
sudo ./pulseview
~~~

Više informacija na [linku](https://sigrok.org/wiki/Linux#PulseView).

# Povezivanje i uspostavljanje komunikacije

Komunikacija korištenjem SPI i I2C interfejsa ostvaruje se povezivanjem modula sa senzorom za mjerenje ubrzanja ***ADXL345*** i ***Raspberry Pi*** platforme.
Pinovi ADXL345 sa oznakama dati su na slici:

![ADXL345](./imgs/adxl.jpg)


## SPI (Serial Peripheral Interface)

- ### Povezivanje

	U slučaju povezivanja SPI interfejsa korištenjem ADXL345 i Raspberry Pi platforme pinovi od interesa na senzoru ADXL345 su:

	- **SCK**/**SCL**: signal serijskog takta
	- **MOSI**/**SDA**: signal MOSI za podatke (od master ka slave uređaju)
	- **MISO**/**ALT**: signal MISO za podatke (od slave ka master uređaju)
	- **CS**: slave select(chip select) signal za omogućenje SPI prenosa


	
	Njih je potrebno povezati sa odgovarajućim pinovima Raspberry Pi platforme na sljedeći način:

	- SCK/SCL - Raspberry Pi pin `23`
	- MOSI/SDA - Raspberry Pi pin `19`
	- MISO/ALT - Raspberry Pi pin `21`
	- CS - Raspberry Pi pin `26` (u slučaju korištenja virtuelnog fajla  `/dev/spidev0.1 `)



	Preostala dva pina, GND (masa) i 3.3V, se mogu povezati na Raspberry Pi platformu na sljedeći način:

	- **GND** - Raspberry Pi pin `39`
	- **3.3V** - Raspberry Pi pin `1`

	oni se povezuju posljednji (prvo masa pa onda napajanje).



	Na slici su prikazani pinovi Raspberry Pi platforme korišteni za realizaciju komunikacije preko SPI interfejsa:


	![RPISPI](./imgs/RPISPI.png)


- ### SPI i PulseView

	Nakon završenog povezivanja akcelerometra ADXL345 i Raspberry Pi platforme iz prethodne tačke potrebno je izvršiti povezivanje osciloskopa sa računarom korištenjem USB kabla.
	Ukoliko se koristi razvojna Linux platforma instalirana na virtuelnoj mašini, što je ovdje slučaj, prilikom priključivanja osciloskopa na računaru će se pojaviti pop-up koji nudi opcije povezivanja na host kao prvu opciju ili virtuelnu mašinu kao drugu opciju. Potrebno je izabrati drugu opciju, odnosno povezati osciloskop sa virtuelnom mašinom.

	Ukoliko je povezivanje izvršeno uspješno potrebno je prvu sondu osciloskopa (Channel 1-*CH1*) povezati na SCK/SCL (pin `23`), a drugu (Channel 2-*CH2*) na MISO/ALT (pin `21`).

	Sada je potrebno pokrenuti PulsView pokretanjem terminala i izvršavanjem komandi:

	~~~
	cd SigrokPulseView/pulseview
	sudo ./pulseview
	~~~

	Kada se program pokrene u gornjem dijelu prozora bi se trebalo pojaviti ime osciloskopa(ako je povezan pravilno) sa kojim je računar povezan. U ovom slučaju to je `Rigol DS1052D`. (U opštem slučaju ako softver automatski ne otkrije povezani uređaj, potrebno je u istom tom prozoru izabrati tip uređaja, a zatim pokrenuti pretragu pomoću koje će uređaj biti otkriven i biće omogućeno povezivanje.)
	Na radnoj površini će biti vidljive dvije linije za prikaz signala označene sa `CH1` (prikaz prvog kanala osciloskopa) i `CH2` (prikaz drugog kanala osciliskopa).
	Klikom na opciju `Add protocol decoder` sa desne strane se otvara prozor `Decoder selector`. U polje za pretragu treba unijeti `spi` i iz ponuđenih opcija dvostrukim klikom lijevog tastera miša izabrati `SPI  Serial Peripheral Interface`. Ispod linija `CH1` i `CH2` će se pojaviti treća linija pod nazivom `SPI`.

	![Screen](./imgs/Screen.jpg)

	![SPIprotdec](./imgs/SPIprotdec.jpg)

	Klikom lijevog tastera miša na imena navedenih linija (CH1, CH2, SPI) otvaraju se prozori za podešavanje linija. Potrebno je još izvršiti podešavanje ovih linija na sljedeći način:

	- CH1:

	![CH1](./imgs/spich1.jpg)

	- CH2:

	![CH2](./imgs/spich2.jpg)


	Da bi bilo moguće podešavanje SPI linije potrebno je ulazne analogne signale konvertovati u digitalne impulse. To ćemo uraditi tako što za opciju `Conversion` izaberemo `to logic via threshold`, dok za opciju `Conversion threshold(s)` postavimo `Signal average`. Opcija `Show traces for` služi za izbor prikaza na signalnim linijama, odnosno omogućava korisniku da izabere da li će biti prikazan analogni signal, digitalni (konvertovani) signal ili i jedan i drugi (što je ovdje slučaj).
	U polju `Name` moguće je unijeti ime koje bi opisivalo signal koji linija predstavlja (npr.: Umjesto **CH1** moguće je unijeti **Clock** jer je na datom kanalu osciloskopa priključen takt signal).

	- SPI:

	![SPI](./imgs/spispi.jpg)


	Pri podešavanju SPI linije potrebno je navedene signale (CLK, MISO, MOSI, CS) povezati sa odgovarajućim kanalima osciloskopa. Kako su korišteni samo signali **CLK** i **MISO**, onda je njih potrebno postaviti tako da signalu **CLK** odgovara **CH1**, a signalu **MISO** odgovara **CH2**.
	Aplikacija koja se pokreće sa Raspberry Pi platforme koristi *SPI mode 3* što znači da se parametri `Clock polarity` (CPOL) i `Clock phase` (CPHA) postavljaju na vrijednost `1`.



	Nakon završenog podešavanja lijevim tasterom miša kliknuti na opciju `Run` u gornjem lijevom uglu čime su PulseView i osciliskop spremni da uhvate signale komunikacije.

	Sa Raspberry Pi platforme se pokreće program pod nazivom `spi` (realizovan tokom laboratorijskih vježbi), nakon čega se na osciloskopu i radnoj površini PulsView softvera pojavljuju talasni oblici SCK/SCL (CH1) i MISO (CH2) signala.

	Na SPI liniji pojavile su se dvije podlinije:

	 - MISO bits (predstavlja pojedinačne bite SPI poruke) i
	 - MISO data (u ovom slučaju predstavlja `Device ID` senzora u ulozi *slave* uređaja)

	Kada se postignu očekivani rezultati simulacije, možemo kliknuti na opciju `Stop` u gornjem lijevom uglu.

	![SPIcapture](./imgs/SPIcapture.jpg)

	


## I2C (Inter-Integrated Circuit)	

 - ### Povezivanje

	U slučaju povezivanja I2C interfejsa korištenjem ADXL345 i Raspberry Pi platforme pinovi od interesa na senzoru ADXL345 su:

	- **SCK**/**SCL**: signal serijskog takta
	- **MOSI**/**SDA**: signal serijskog podatka
	- **MISO**/**ALT**: signal ALT za selekciju slave uređaja
	- **CS**
	
	Njih je potrebno povezati sa odgovarajućim pinovima Raspberry Pi platforme na sljedeći način:

	- MOSI/SDA - Raspberry Pi pin `3`
	- SCK/SCL - Raspberry Pi pin `5`
	- MISO/ALT - Raspberry Pi pin `6`
	- CS - Raspberry Pi pin `17`

	Signal ALT, koji se nalazi na senzoru, povezuje se sa pinom za masu na Raspberry Pi platformoi, dok se CS povezuje na visok logički nivo, tj. na 3.3V.

	Preostala dva pina, GND (masa) i 3.3V, se mogu povezati na Raspberry Pi platformu na sljedeći način:

	- **GND** - Raspberry Pi pin `39`
	- **3.3V** - Raspberry Pi pin `1`

	oni se povezuju posljednji (prvo masa pa onda napajanje).


	Na slici su prikazani pinovi Raspberry Pi platforme korišteni za realizaciju komunikacije preko I2C interfejsa:

	![RPII2C](./imgs/RPII2C.png)

- ### I2C i PulseView

	Prije početka rada potrebno je omogućiti I2C interfejs na Raspberry Pi platformi korišćenjem alatke `raspi-config` ili editovanjem fajla `/boot/config.txt` (tako da postoji linija `dtparam=i2c_arm=on`). Ovo je neophodno samo ako I2C interfejs već nije omogućen, što se može provjeriti komandom `dmesg | grep i2c`.

	Nakon završenog povezivanja akcelerometra ADXL345 i Raspberry Pi platforme iz prethodne tačke potrebno je izvršiti povezivanje osciloskopa sa računarom korištenjem USB kabla.

	**Napomena:** Povezivanje osciloskopa sa virtuelnom mašinu se obavlja na identičan način kao u slučaju rada sa SPI interfejsom.

	Ukoliko je povezivanje izvršeno uspješno potrebno je prvu sondu osciloskopa (Channel 1-*CH1*) povezati na Clock (pin `3`), a drugu (Channel 2-*CH2*) na Data (pin `5`).

	Sada je potrebno pokrenuti PulsView pokretanjem terminala i izvršavanjem komandi:

	~~~
	cd SigrokPulseView/pulseview
	sudo ./pulseview
	~~~

	Kada se program pokrene u gornjem dijelu prozora bi se trebalo pojaviti ime osciloskopa(ako je povezan pravilno) sa kojim je računar povezan. U ovom slučaju to je `Rigol DS1052D`. (U opštem slučaju ako softver automatski ne otkrije povezani uređaj, potrebno je u istom tom prozoru izabrati tip uređaja, a zatim pokrenuti pretragu pomoću koje će uređaj biti otkriven i biće omogućeno povezivanje.)
	Na radnoj površini će biti vidljive dvije linije za prikaz signala označene sa `CH1` (prikaz prvog kanala osciloskopa) i `CH2` (prikaz drugog kanala osciliskopa).
	Klikom na opciju `Add protocol decoder` sa desne strane se otvara prozor `Decoder selector`. U polje za pretragu treba unijeti `i2c` i iz ponuđenih opcija dvostrukim klikom lijevog tastera miša izabrati `I2C  Inter-Integrated Circuit`. Ispod linija `CH1` i `CH2` će se pojaviti treća linija pod nazivom `I2C`.

	![Screen](./imgs/Screen.jpg)

	![I2Cprotdec](./imgs/I2Cprotdec.jpg)


	Klikom lijevog tastera miša na imena navedenih linija (CH1, CH2, I2C) otvaraju se prozori za podešavanje linija. Potrebno je još izvršiti podešavanje ovih linija na sljedeći način:

	**Napomena:** `CH1` i `CH2` se podešavaju na isti način kao kod SPI interfejsa.

	
	- I2C:

	![I2C](./imgs/i2ci2c.jpg)

	Slično kao kod SPI interfejsa, navedenim signalima (SCL i SDA) potrebno je dodijeliti odgovarajuće kanale osciloskopa. Signalu **SCL** se dodjeljuje **CH1**, a signalu **SDA** se dodjeljuje **CH2**.
	Za opciju `Stack Decoder` izabrati `DS1307`.


	Nakon završenog podešavanja lijevim tasterom miša kliknuti na opciju `Run` u gornjem lijevom uglu čime su PulseView i osciliskop spremni da uhvate signale komunikacije.

	Sa Raspberry Pi platforme se pokreće program pod nazivom `i2c` (realizovan tokom laboratorijskih vježbi), nakon čega se na osciloskopu i radnoj površini PulsView softvera pojavljuju talasni oblici Clock (CH1) i Data (CH2) signala.

	Na I2C liniji pojavile su se dvije podlinije:

	 - I2C bits (predstavlja pojedinačne bite I2C poruke) i
	 - I2C Address/data (u ovom slučaju predstavlja komunikaciju senzora u ulozi *slave* uređaja sa Raspberry Pi platformom)

	Kada se postignu očekivani rezultati simulacije, možemo kliknuti na opciju `Stop` u gornjem lijevom uglu.

	![I2Ccapture](./imgs/I2Ccapture.jpg)

	
